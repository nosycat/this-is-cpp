#include <iostream>
#include <variant>

int main() {
	std::variant<int, char> data = 65;
	std::cout << std::get<int>(data) << " is ";
	data = 'A';
	std::cout << std::get<char>(data) << std::endl;
	return 0;
}
