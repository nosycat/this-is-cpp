#include <iostream>
#include <string>

int main() {
	std::cout << "What's your name? ";

	std::string name;

	if (std::getline(std::cin, name))
		std::cout << "Nice to meet you, " << name << "!";
	else
		std::cout << "OK, bye.";
	std::cout << std::endl;
	return 0;
}
