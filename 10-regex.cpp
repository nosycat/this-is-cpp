#include <iostream>
#include <string>
#include <regex>
 
int main() {
	const std::string data = ":123abc45def678gh+ij";
	std::regex re_token("\\d+|[[:alpha:]]+");

	auto start = std::sregex_iterator(
		data.begin(), data.end(), re_token);
	auto finish = std::sregex_iterator();

	for (auto& i = start; i != finish; i++)
		std::cout << i->str() << "\n";
}
