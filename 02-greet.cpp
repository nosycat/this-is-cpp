#include <iostream>

int main(int argc, char **argv) {
	if (argc > 1)
		std::cout << "Hello, " << argv[1] << "!";
	else
		std::cout << "Hello, world!";
	std::cout << std::endl;
	return 0;
}
