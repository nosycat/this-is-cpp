#include <iostream>
#include <string>

int main(int argc, char **argv) {
	auto greet = [](const std::string& who) {
		std::cout << "Hello, " << who << "!" << std::endl;
	};
	greet(argc > 1 ? argv[1] : "world");
	return 0;
}
