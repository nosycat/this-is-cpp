#include <iostream>
#include <string>
#include <vector>

using Str = std::string;
using Vec = std::vector<Str>;

int main() {
	Vec sentence;
	Str word;
	
	std::cout << "Enter a few words (bye to end): ";
	while (std::cin >> word) {
		if (word == "bye")
			break;
		else
			sentence.push_back(word);
	}	
	std::cout << "You entered " << sentence.size() << " words.\n";
}
