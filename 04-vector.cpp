#include <iostream>
#include <string>
#include <vector>

int main() {
	std::vector<std::string> rhyme{"eeny", "meeny", "miny", "moe"};
	for (const auto& i: rhyme)
		std::cout << i << ", ";
	std::cout << "..." << std::endl;
}
