#include <iostream>
#include <fstream>
#include <string>

int main() {
	const std::string fn = "test.txt";
	{
		std::ofstream sink(fn);
		if (sink.is_open()) {
			sink << "Hello, world!" << std::endl;
		} else {
			std::cerr << "Output file error" << std::endl;
			return 1;
		}
	}
	std::cout << "Reading back data..." << std::endl;
	{
		std::ifstream source(fn);
		std::string input;
		if (source.is_open()) {
			if (std::getline(source, input))
				std::cout << input << std::endl;
			else
				std::cerr << "Read error" << std::endl;
		} else {
			std::cerr << "Output file error" << std::endl;
			return 2;
		}
	}
}
