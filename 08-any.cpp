#include <iostream>
#include <string>
#include <any>

int main() {
	std::any data = 42;
	std::cout << std::any_cast<int>(data);
	data = std::string(" is The Answer.");
	std::cout << std::any_cast<std::string>(data) << std::endl;
}
